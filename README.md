test-production-sites
=====================

Run Behat tests against production WWU Drupal sites.

#Arguments

1. BEHAT_DIR: full path to the directory containing the behat.yml to be used to run tests.
2. SITES: A list of strings containing the profile names to be tested e.g. "site1 site2 site3"
