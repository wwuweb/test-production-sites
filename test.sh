#!/bin/bash

BEHAT_DIR=$1
SITES=$2
SUITE=$3

let EXIT=0

for SITE in $SITES
do
  echo "Running tests against ${SITE}"
  behat --config ${BEHAT_DIR}/behat.yml --profile $SITE --suite $SUITE

  if [ $? -ne 0 ]
    then EXIT=1
  fi
done

exit $EXIT
